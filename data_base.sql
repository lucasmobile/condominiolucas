-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.24-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para fluccas
DROP DATABASE IF EXISTS `fluccas`;
CREATE DATABASE IF NOT EXISTS `fluccas` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `fluccas`;

-- Copiando estrutura para tabela fluccas.fluccas_administradoras
DROP TABLE IF EXISTS `fluccas_administradoras`;
CREATE TABLE IF NOT EXISTS `fluccas_administradoras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_administradora` varchar(255) NOT NULL DEFAULT '',
  `cnpj` varchar(14) NOT NULL DEFAULT '',
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_administradoras: ~5 rows (aproximadamente)
DELETE FROM `fluccas_administradoras`;
/*!40000 ALTER TABLE `fluccas_administradoras` DISABLE KEYS */;
INSERT INTO `fluccas_administradoras` (`id`, `nome_administradora`, `cnpj`, `data_cadastro`) VALUES
	(8, 'fluccas condominios', '07.236.583/000', '2022-04-09 20:15:14'),
	(9, 'kaliel philipepi ', '79.182.920/000', '2022-04-09 20:30:53'),
	(10, 'molho subways', '76.722.447/000', '2022-04-09 20:16:02'),
	(14, 'teste', 'teste', '2022-04-09 21:52:39'),
	(16, 'fluccas condominios', '85.404.671/000', '2022-04-10 18:41:32');
/*!40000 ALTER TABLE `fluccas_administradoras` ENABLE KEYS */;

-- Copiando estrutura para tabela fluccas.fluccas_blocos
DROP TABLE IF EXISTS `fluccas_blocos`;
CREATE TABLE IF NOT EXISTS `fluccas_blocos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_bloco` varchar(35) NOT NULL DEFAULT '',
  `qt_andares` tinyint(4) NOT NULL DEFAULT 0,
  `unidades_andar` tinyint(4) NOT NULL DEFAULT 0,
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_condominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_blocos: ~4 rows (aproximadamente)
DELETE FROM `fluccas_blocos`;
/*!40000 ALTER TABLE `fluccas_blocos` DISABLE KEYS */;
INSERT INTO `fluccas_blocos` (`id`, `nome_bloco`, `qt_andares`, `unidades_andar`, `data_cadastro`, `from_condominio`) VALUES
	(17, 'belomont', 45, 1, '2022-04-11 21:16:39', 61),
	(18, 'vaisefude', 12, 12, '2022-04-11 21:16:32', 61),
	(19, 'q nojo', 50, 50, '2022-04-11 21:13:52', 61),
	(20, 'badrequest404', 12, 12, '2022-04-11 21:16:37', 61);
/*!40000 ALTER TABLE `fluccas_blocos` ENABLE KEYS */;

-- Copiando estrutura para tabela fluccas.fluccas_clientes
DROP TABLE IF EXISTS `fluccas_clientes`;
CREATE TABLE IF NOT EXISTS `fluccas_clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL,
  `telefone` varchar(30) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_unidade` int(11) NOT NULL,
  `from_bloco` int(11) NOT NULL,
  `from_condominio` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chCondominio` (`from_condominio`),
  KEY `chBloco` (`from_bloco`),
  KEY `chUnidade` (`from_unidade`),
  CONSTRAINT `chBloco` FOREIGN KEY (`from_bloco`) REFERENCES `fluccas_blocos` (`id`),
  CONSTRAINT `chCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `fluccas_condominios` (`id`),
  CONSTRAINT `chUnidade` FOREIGN KEY (`from_unidade`) REFERENCES `fluccas_unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_clientes: ~3 rows (aproximadamente)
DELETE FROM `fluccas_clientes`;
/*!40000 ALTER TABLE `fluccas_clientes` DISABLE KEYS */;
INSERT INTO `fluccas_clientes` (`id`, `nome`, `cpf`, `email`, `telefone`, `endereco`, `data_cadastro`, `from_unidade`, `from_bloco`, `from_condominio`) VALUES
	(11, 'asdasd', '123123', 'asdasd', '123123', 'asdasd', '2022-04-09 20:34:03', 30, 17, 61),
	(12, 'teste', '1', 'teste', '1', 'teste', '2022-04-09 21:54:16', 30, 17, 61),
	(13, 'bad request 404', '12323121', 'bad request', '21121212', 'bad request', '2022-04-11 20:02:48', 30, 17, 61);
/*!40000 ALTER TABLE `fluccas_clientes` ENABLE KEYS */;

-- Copiando estrutura para tabela fluccas.fluccas_condominios
DROP TABLE IF EXISTS `fluccas_condominios`;
CREATE TABLE IF NOT EXISTS `fluccas_condominios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_condominio` varchar(255) NOT NULL DEFAULT '',
  `qt_blocos` tinyint(4) NOT NULL DEFAULT 0,
  `endereco_condominio` varchar(255) NOT NULL DEFAULT '0',
  `cep_condominio` varchar(8) NOT NULL DEFAULT '0',
  `estado_condominio` varchar(30) NOT NULL DEFAULT '0',
  `cidade_condominio` varchar(35) NOT NULL DEFAULT '0',
  `bairro_condominio` varchar(35) NOT NULL DEFAULT '0',
  `logradouro_condominio` varchar(35) NOT NULL DEFAULT '0',
  `numero_condominio` varchar(8) NOT NULL DEFAULT '0',
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_administradoras` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fluccas_condominios_fluccas_administradoras` (`from_administradoras`),
  CONSTRAINT `FK_fluccas_condominios_fluccas_administradoras` FOREIGN KEY (`from_administradoras`) REFERENCES `fluccas_administradoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_condominios: ~2 rows (aproximadamente)
DELETE FROM `fluccas_condominios`;
/*!40000 ALTER TABLE `fluccas_condominios` DISABLE KEYS */;
INSERT INTO `fluccas_condominios` (`id`, `nome_condominio`, `qt_blocos`, `endereco_condominio`, `cep_condominio`, `estado_condominio`, `cidade_condominio`, `bairro_condominio`, `logradouro_condominio`, `numero_condominio`, `data_cadastro`, `from_administradoras`) VALUES
	(61, 'bad request', 2, 'Rua Adécio Gonçalves', '89045230', 'AC', 'Blumenau', 'Velha', 'Casa', '260', '2022-04-11 19:54:47', 8),
	(65, 'asdasd', 127, 'asdasdas', '231312', 'BA', 'asdasd', 'asdas', 'dasdas', '123', '2022-04-10 18:41:48', 8);
/*!40000 ALTER TABLE `fluccas_condominios` ENABLE KEYS */;

-- Copiando estrutura para tabela fluccas.fluccas_conselho
DROP TABLE IF EXISTS `fluccas_conselho`;
CREATE TABLE IF NOT EXISTS `fluccas_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `funcao` enum('Sindico','SubSindico','Conselheiro') DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_condominio` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fluccas_conselho_fluccas_condominios` (`from_condominio`),
  CONSTRAINT `FK_foreign_key` FOREIGN KEY (`from_condominio`) REFERENCES `fluccas_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_conselho: ~3 rows (aproximadamente)
DELETE FROM `fluccas_conselho`;
/*!40000 ALTER TABLE `fluccas_conselho` DISABLE KEYS */;
INSERT INTO `fluccas_conselho` (`id`, `nome`, `funcao`, `data_cadastro`, `from_condominio`) VALUES
	(51, 'sindico', 'Sindico', '2022-04-10 19:24:38', 65),
	(52, 'subsindico', 'SubSindico', '2022-04-10 19:24:47', 65),
	(54, 'conselheiro', 'Conselheiro', '2022-04-10 19:24:59', 65),
	(94, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 21:56:29', 61),
	(95, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 21:57:31', 61),
	(96, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 21:57:31', 61),
	(97, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 21:59:37', 61),
	(98, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 21:59:37', 61),
	(99, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:01:01', 61),
	(100, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:01:01', 61),
	(101, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:01:25', 61),
	(102, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:01:25', 61),
	(103, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:02:27', 61),
	(104, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:02:57', 61),
	(105, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:03:57', 61),
	(106, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:03:57', 61),
	(107, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:06:07', 61),
	(108, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:06:07', 61),
	(109, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:07:45', 61),
	(110, 'Lucas Silva de Oliveira', 'Sindico', '2022-04-11 22:07:45', 61);
/*!40000 ALTER TABLE `fluccas_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela fluccas.fluccas_lista_convidados
DROP TABLE IF EXISTS `fluccas_lista_convidados`;
CREATE TABLE IF NOT EXISTS `fluccas_lista_convidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_convidado` varchar(255) DEFAULT '',
  `cpf_convidado` varchar(11) DEFAULT '',
  `celular_convidado` varchar(30) DEFAULT '',
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_reserva_salao_festas` int(11) DEFAULT NULL,
  `from_unidade` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__fluccas_reserva_salao_festas` (`from_reserva_salao_festas`),
  KEY `FK__fluccas_unidades` (`from_unidade`),
  CONSTRAINT `FK__fluccas_reserva_salao_festas` FOREIGN KEY (`from_reserva_salao_festas`) REFERENCES `fluccas_reserva_salao_festas` (`id`),
  CONSTRAINT `FK__fluccas_unidades` FOREIGN KEY (`from_unidade`) REFERENCES `fluccas_unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_lista_convidados: ~0 rows (aproximadamente)
DELETE FROM `fluccas_lista_convidados`;
/*!40000 ALTER TABLE `fluccas_lista_convidados` DISABLE KEYS */;
/*!40000 ALTER TABLE `fluccas_lista_convidados` ENABLE KEYS */;

-- Copiando estrutura para tabela fluccas.fluccas_pets
DROP TABLE IF EXISTS `fluccas_pets`;
CREATE TABLE IF NOT EXISTS `fluccas_pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pet` varchar(255) DEFAULT NULL,
  `tipo` enum('Cachorro','Gato','Passarinho') DEFAULT NULL,
  `from_morador` int(11) DEFAULT 0,
  `data_cadastro` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_fluccas_pets_fluccas_clientes` (`from_morador`),
  CONSTRAINT `FK_fluccas_pets_fluccas_clientes` FOREIGN KEY (`from_morador`) REFERENCES `fluccas_clientes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_pets: ~0 rows (aproximadamente)
DELETE FROM `fluccas_pets`;
/*!40000 ALTER TABLE `fluccas_pets` DISABLE KEYS */;
/*!40000 ALTER TABLE `fluccas_pets` ENABLE KEYS */;

-- Copiando estrutura para tabela fluccas.fluccas_reserva_salao_festas
DROP TABLE IF EXISTS `fluccas_reserva_salao_festas`;
CREATE TABLE IF NOT EXISTS `fluccas_reserva_salao_festas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_evento` varchar(255) DEFAULT NULL,
  `datahora_evento` timestamp NULL DEFAULT NULL,
  `data_cadastro` timestamp NULL DEFAULT NULL,
  `from_unidade` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fluccas_reserva_salao_festas_fluccas_unidades` (`from_unidade`),
  CONSTRAINT `FK_fluccas_reserva_salao_festas_fluccas_unidades` FOREIGN KEY (`from_unidade`) REFERENCES `fluccas_unidades` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_reserva_salao_festas: ~0 rows (aproximadamente)
DELETE FROM `fluccas_reserva_salao_festas`;
/*!40000 ALTER TABLE `fluccas_reserva_salao_festas` DISABLE KEYS */;
/*!40000 ALTER TABLE `fluccas_reserva_salao_festas` ENABLE KEYS */;

-- Copiando estrutura para tabela fluccas.fluccas_unidades
DROP TABLE IF EXISTS `fluccas_unidades`;
CREATE TABLE IF NOT EXISTS `fluccas_unidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_unidade` int(11) NOT NULL DEFAULT 0,
  `metragem_unidade` float NOT NULL DEFAULT 0,
  `qt_vagas_garagem` tinyint(4) NOT NULL DEFAULT 0,
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `from_bloco` int(11) DEFAULT NULL,
  `from_condominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_unidades: ~2 rows (aproximadamente)
DELETE FROM `fluccas_unidades`;
/*!40000 ALTER TABLE `fluccas_unidades` DISABLE KEYS */;
INSERT INTO `fluccas_unidades` (`id`, `numero_unidade`, `metragem_unidade`, `qt_vagas_garagem`, `data_cadastro`, `from_bloco`, `from_condominio`) VALUES
	(30, 1, 50, 15, '2022-04-09 20:33:40', 17, 61),
	(31, 1, 2, 1, '2022-04-11 21:17:09', 18, 0);
/*!40000 ALTER TABLE `fluccas_unidades` ENABLE KEYS */;

-- Copiando estrutura para tabela fluccas.fluccas_users
DROP TABLE IF EXISTS `fluccas_users`;
CREATE TABLE IF NOT EXISTS `fluccas_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `user_name` varchar(50) NOT NULL DEFAULT '0',
  `user_password` varchar(50) NOT NULL DEFAULT '0',
  `data_cadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fluccas.fluccas_users: ~3 rows (aproximadamente)
DELETE FROM `fluccas_users`;
/*!40000 ALTER TABLE `fluccas_users` DISABLE KEYS */;
INSERT INTO `fluccas_users` (`id`, `name`, `user_name`, `user_password`, `data_cadastro`) VALUES
	(1, 'Lucas', 'jão', '698dc19d489c4e4db73e28a713eab07b', '2022-04-10 18:46:42'),
	(5, 'kaliel', 'vagabunda', '202cb962ac59075b964b07152d234b70', '2022-04-09 20:25:30'),
	(6, 'Administrador', 'adm', 'b09c600fddc573f117449b3723f23d64', '2022-04-10 18:32:48');
/*!40000 ALTER TABLE `fluccas_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
