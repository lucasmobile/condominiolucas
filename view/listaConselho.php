<center>
    <h2 class="mt-5">Lista de Conselho</h2>
</center>
<div class="row">
    <div class="col-12">
        <table class="table table-hover" id="tableConselho">
            <thead class="bg-dark text-light">
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Função</th>
                    <th scope="col">Data Cadastro</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($result['resultSet'] as $value) { ?>
                    <tr data-id="<?= $value['id'] ?>">
                        <td><?=$value['nome']?></td>
                        <td><?=$value['funcao']?></td>
                        <td><?=dateFormat($value['data_cadastro']);?></td>
                        <td>
                            <a href="#" data-id="<?= $value['id'] ?>" class="removerConselho"><i class="bi bi-x-octagon text-dark"></i></a>
                            <a href="<?= $url_site ?>cadastroConselho/id/<?= $value['id'] ?>"><i class="bi bi-pencil-square text-dark"></i></a>
                        </td>
                    </tr>
                <? } ?>
                    <tr>
                        <td colspan="3"></td>
                        <td class="totalRegistros">Total de Registros: <?=($result['totalResults'] < 10 ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row mt-5 mb-5">
    <div class="col-12">
        <a class="btn btn-dark text-light mb-5" href="<?=$url_site?>cadastroConselho">Cadastrar novo conselho</a>
    </div>
</div>
<div class="col-sm-12 mb-5">
    <?= $paginacao ?>
</div>