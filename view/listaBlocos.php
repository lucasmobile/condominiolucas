<center>
    <h2 class="mt-5">Lista de Blocos</h2>
</center>
<form method="GET" id="filtro">
    <div class="row">
        <div class="col-9 col-md-9">
            <input type="hidden" name="page" value="listaBlocos">
            <input class="form-control termo1" type="text" name="b[termo1]" placeholder="Filtrar por nome">
        </div>
        <div class="col-3 col-md-3">
            <button type="submit" class="btn btn-dark text-light mb-5" disabled>Filtrar</button>
            <a class="btn btn-outline-dark btn-light text-dark mb-5" href="">Limpar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <select class="custom-select termo2" name="b[termo2]">
                <option value="">Filtrar Condominio</option>
                <?
                    foreach($listagemCondominio['resultSet'] as $cond) {
                        echo '<option value="' . $cond['id'] . '">' . $cond['nome_condominio'] . '</option>';
                    }
                ?>
            </select>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-12">
        <table class="table table-hover" id="tableBlocos">
            <thead class="bg-dark text-light">
                <tr>
                    <th scope="col">Condomínio</th>
                    <th scope="col">Nome Bloco</th>
                    <th scope="col">Andares</th>
                    <th scope="col">Unidades por andar</th>
                    <th scope="col">Data de Cadastro</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($result['resultSet'] as $value) { 
                    ?>
                    <tr data-id="<?= $value['id'] ?>">
                        <td><?=$value['nome_condominio']?></td>
                        <td><?=$value['nome_bloco']?></td>
                        <td><?=$value['qt_andares']?></td>
                        <td><?=$value['unidades_andar']?></td>
                        <td><?=dateFormat($value['data_cadastro']);?></td>
                        <td>
                        <a href="#" data-id="<?= $value['id'] ?>" class="removerBloco"><i class="bi bi-x-octagon text-dark"></i></a>
                        <a href="<?=$url_site?>cadastroBloco/id/<?= $value['id'] ?>"><i class="bi bi-pencil-square text-dark"></i></a>
                        </td>
                    </tr>
                <? } ?>
                    <tr>
                        <td colspan="5"></td>
                        <td class="totalRegistros">Total de Registros: <?=($result['totalResults'] < 10 ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row mt-5 mb-5">
    <div class="col-12">
        <a class="btn btn-dark text-light mb-5" href="<?=$url_site?>cadastroBloco">Cadastrar novo bloco</a>
    </div>
</div>
<div class="col-sm-12 mb-5">
    <?= $paginacao ?>
</div>