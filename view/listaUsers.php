<center>
    <h2 class="mt-5">Lista de Usuários</h2>
</center>
<div class="row mt-5">
    <div class="col-10">
        <form action="index.php" method="GET">
            <input type="hidden" name="page" value="listaUsers">
            <input class="form-control termo1" type="text" name="buscar" placeholder="Filtro...">
        </div>
        <div class="col-2">
            <button type="submit" class="btn btn-dark text-light mb-5">Filtrar</button>
        </div>
        </form>
</div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-hover" id="tableUsers">
            <thead class="bg-dark text-light">
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">User Name</th>
                    <th scope="col">Data Cadastro</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($result['resultSet'] as $key => $value) { ?>
                    <tr data-id="<?= $value['id'] ?>">
                        <td><?=$value['name']?></td>
                        <td><?=$value['user_name']?></td>
                        <td><?= dateFormat($value['data_cadastro']) ?></td>
                        <td>
                            <a href="#" data-id="<?= $value['id'] ?>" class="removerUser"><i class="bi bi-x-octagon text-dark"></i></a>
                            <a href="<? $url_site ?>cadastroUser/id/<?= $value['id'] ?>"><i class="bi bi-pencil-square text-dark"></i></a>
                        </td>
                    </tr>
                <? } ?>
                    <tr>
                        <td colspan="3"></td>
                        <td class="totalRegistros">Total de Registros: <?=($result['totalResults'] < 10 ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row mt-5">
    <div class="col-12">
        <a class="btn btn-dark text-light mb-5" href="<?=$url_site?>cadastroUser">Cadastrar novo usuário</a>
    </div>
</div>
<div class="col-sm-12 mb-5">
    <?= $paginacao ?>
</div>