<center>
    <h2 class="mt-5 mb-5">Cadastro de Clientes</h2>
</center>
<form id="formClientes" action="" method="POST">
    <div class="row mb-5">
        <div class="col-12 col-sm-6 col-md-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Condominio</label>
                </div>
                <select class="custom-select fromCondominio" id="inputGroupSelect01" name="from_condominio">
                    <option value="">Selecione</option>
                    <?
                    
                    foreach ($listagemCondominio['resultSet'] as $condominio) {
                        
                        echo '<option value="' . $condominio['id'] . '" ' . ($condominio['id'] == $listagemCadastro['resultSet']['from_condominio'] ? 'selected' : '') . '>' . $condominio['nome_condominio'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Bloco</label>
                </div>
                <select class="custom-select fromBloco" id="inputGroupSelect01" name="from_bloco">
                <option value="">Selecione</option>
                    <?
                    foreach ($listagemBloco['resultSet'] as $bloco) {
                        echo '<option value="' . $bloco['id'] . '" ' . ($bloco['id'] == $listagemCadastro['resultSet']['from_bloco'] ? 'selected' : '') . '>' . $bloco['nome_bloco'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Unidade</label>
                </div>
                <select class="custom-select fromUnidade" id="inputGroupSelect01" name="from_unidade">
                <option value="">Selecione</option>
                    <?
                    foreach ($listagemUnidade['resultSet'] as $unidade) {
                        echo '<option value="' . $unidade['id'] . '" ' . ($unidade['id'] == $listagemCadastro['resultSet']['from_unidade'] ? 'selected' : '') . '>' . $unidade['numero_unidade'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 col-sm-9 col-md-9">
            <input class="form-control" type="text" value="<?= $listagemCadastro['resultSet']['nome'] ?>" name="nome" id="nome" placeholder="Nome" required>
        </div>
        <div class="col-12 col-sm-3 col-md-3">
            <input class="form-control" type="text" value="<?= $listagemCadastro['resultSet']['cpf'] ?>" name="cpf" id="cpf" placeholder="CPF">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 col-sm-9 col-md-8">
            <input class="form-control" type="text" value="<?= $listagemCadastro['resultSet']['email'] ?>" name="email" id="email" placeholder="Email" required>
        </div>
        <div class="col-12 col-sm-3 col-md-4">
            <input class="form-control" type="text" value="<?= $listagemCadastro['resultSet']['telefone'] ?>" name="telefone" id="telefone" placeholder="Telefone">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12">
            <input class="form-control" type="text" value="<?= $listagemCadastro['resultSet']['endereco'] ?>" name="endereco" id="endereco" placeholder="Endereço">
        </div>
    </div>
    <? if ($_GET['id']) { ?>
        <input type="hidden" id="editar" name="editar" value="<?= $_GET['id'] ?>">
    <? } ?>
    <div class="row">
        <div class="col-12">
            <center><button class="btn btn-dark text-light buttonEnviar" type="submit" style="width: 100%;">Enviar</button></center>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-5">
            <a href="<?=$url_site?>listaClientes">Ver Lista de Clientes</a>
        </div>
    </div>
</form>