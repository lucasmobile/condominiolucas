<center>
    <h1 class="mb-5 mt-5">Cadastro de Usuários</h1>
</center>

<form id="formUsers" action="" method="POST">
    <div class="row mb-5">
        <div class="col-12 col-sm-9 col-md-3 mb-3">
            <input class="form-control" type="text" value="<?= $listagemUsers['resultSet']['name'] ?>" name="g[name]" placeholder="Nome">
        </div>
        <div class="col-12 col-sm-3 col-md-3 mb-3">
            <input class="form-control" type="text" value="<?= $listagemUsers['resultSet']['user_name'] ?>" name="g[user_name]" placeholder="Nome de Usuário">
        </div>
        <div class="col-12 col-sm-3 col-md-3 mb-3">
            <?= $password ?>
        </div>
        <div class="col-12 col-sm-3 col-md-3 mb-3">
            <?= $retypePassword ?>
        </div>
    </div>
    <? if ($_GET['id']) { ?>
        <input type="hidden" id="editar" name="g[editar]" value="<?= $_GET['id'] ?>">
    <? } ?>
    <div class="row">
        <div class="col-12">
            <center><button class="btn btn-dark text-light buttonEnviar" type="submit" style="width: 100%;">Cadastrar</button></center>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-5">
            <a href="<?=$url_site?>listaUsers">Ver Lista de Usuários</a>
        </div>
    </div>
</form>