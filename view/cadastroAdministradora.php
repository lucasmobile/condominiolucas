<center>
    <h1 class="mb-5 mt-5">Cadastro de Administradoras</h1>
</center>

<form id="formAdministradoras" action="" method="POST">
    <div class="row mb-5">
        <div class="col-12 col-sm-7 col-md-8">
            <input class="form-control" type="text" value="<?=$listagemAdministradora['resultSet']['nome_administradora']?>" name="nome_administradora" placeholder="Nome da Administradora">
        </div>
        <div class="col-12 col-sm-5 col-md-4">
            <input class="form-control" type="text" name="cnpj" value="<?=$listagemAdministradora['resultSet']['cnpj']?>" placeholder="CNPJ">
        </div>
    </div>
    <? if($_GET['id']) { ?>
        <input type="hidden" id="editar" name="editar" value="<?=$_GET['id']?>"> 
    <? } ?>
    <div class="row">
        <div class="col-12">
            <center><button class="btn btn-dark text-light buttonEnviar" type="submit" style="width: 100%;">Cadastrar</button></center>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-5">
            <a href="<?=$url_site?>listaAdministradoras">Ver Lista de Administradoras</a>
        </div>
    </div>
</form>