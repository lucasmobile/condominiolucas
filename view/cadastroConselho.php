<center>
    <h1 class="mb-5 mt-5">Cadastro de Conselho</h1>
</center>

<form id="formConselho" action="" method="POST">
    <div class="row mb-5">
        <div class="col-12">
            <div class="input-group mb-3">
                <select name="from_condominio" class="fromCondominio custom-select form-control mt-2">
                    <option value="">Selecione um Condomínio</option>
                    <? foreach ($listagemCondominio['resultSet'] as $condominios) {
                        echo '<option value="' . $condominios['id'] . '"' . ($condominios['id'] == $condominios['nome_condominio'] ? 'selected' : '') . '>' . $condominios['nome_condominio'] . '</option>';
                    } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 col-sm-7 col-md-8">
            <input class="form-control" type="text" value="<?= $listagemConselho['resultSet']['nome'] ?>" name="nome" placeholder="Nome">
        </div>
        <div class="col-12 col-md-4">
            <select class="custom-select fromConselho" id="inputGroupSelect01" name="funcao">
                <option value="">Função</option>
                <?
                foreach ($listagemFuncao['resultSet'] as $funcao) {
                    echo '<option value="' . $funcao['funcao'] . '"' . ($funcao['funcao'] == $funcao['resultSet'] ? 'selected' : '') . '>' . $funcao['funcao'] . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <? if ($_GET['id']) { ?>
        <input type="hidden" id="editar" name="editar" value="<?= $_GET['id'] ?>">
    <? } ?>
    <div class="row">
        <div class="col-12">
            <center><button class="btn btn-dark text-light buttonEnviar" type="submit" style="width: 100%;">Cadastrar</button></center>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-5">
            <a href="<?= $url_site ?>listaConselho">Ver Lista de Conselho</a>
        </div>
    </div>
</form>