<center>
    <h2 class="mt-5">Lista de Condominios</h2>
</center>


<form method="GET" id="filtro">
    <div class="row">
        <div class="col-9 col-md-9">
            <input type="hidden" name="page" value="listaCondominios">
            <input class="form-control termo1" type="text" name="b[termo1]" placeholder="Filtrar por nome">
        </div>
        <div class="col-3 col-md-3">
            <button type="submit" class="btn btn-dark text-light mb-5" disabled>Filtrar</button>
            <a class="btn btn-outline-dark btn-light text-dark mb-5" href="">Limpar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <select class="custom-select termo2" name="b[termo2]">
                <option value="">Filtrar administradora</option>
                <?
                foreach ($listAdministradoras['resultSet'] as $adm) {
                    echo '<option value="' . $adm['id'] . '">' . $adm['nome_administradora'] . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
</form>


<div class="row">
    <div class="col-12">
        <table class="table table-hover" id="tableCondominios">
            <thead class="bg-dark text-light">
                <tr>
                    <th scope="col">Administradora</th>
                    <th scope="col">Condominio</th>
                    <th scope="col">Qt Blocos</th>
                    <th scope="col">Endereço</th>
                    <th scope="col">CEP</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Cidade</th>
                    <th scope="col">Bairro</th>
                    <th scope="col">Logradouro</th>
                    <th scope="col">Número</th>
                    <th scope="col">Cadastro</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($result['resultSet'] as $value) { ?>
                    <tr data-id="<?= $value['id'] ?>">
                        <td><?= $value['nome_administradora'] ?></td>
                        <td><?= $value['nome_condominio'] ?></td>
                        <td><?= $value['qt_blocos'] ?></td>
                        <td><?= $value['endereco_condominio'] ?></td>
                        <td><?= $value['cep_condominio'] ?></td>
                        <td><?= $value['estado_condominio'] ?></td>
                        <td><?= $value['cidade_condominio'] ?></td>
                        <td><?= $value['bairro_condominio'] ?></td>
                        <td><?= $value['logradouro_condominio'] ?></td>
                        <td><?=  $value['numero_condominio'] ?></td>
                        <td><?= dateFormat($value['data_cadastro']) ?></td>

                        <td>
                            <a href="#" data-id="<?= $value['id'] ?>" class="removerCondominio"><i class="bi bi-x-octagon text-dark"></i></a>
                            <a href="<?=$url_site?>cadastroCondominio/id/<?= $value['id'] ?>"><i class="bi bi-pencil-square text-dark"></i></a>
                        </td>
                    </tr>
                <? } ?>
                <tr>
                    <td colspan="10"></td>
                    <td class="totalRegistros">Total de Registros: <?= $totalRegistros ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row mb-5">
    <div class="col-12">
        <a class="btn btn-dark text-light" href="<?=$url_site?>cadastroCondominio">Cadastrar novo condominio</a>
    </div>
</div>
<div class="col-sm-12 mb-5">
    <?= $paginacao ?>
</div>