<center>
    <h2 class="mt-5">Lista de Clientes</h2>
</center>
<form method="GET" id="filtro">
    <div class="row">
        <div class="col-9 col-md-9">
            <input type="hidden" name="page" value="listaClientes">
            <input class="form-control termo1" type="text" name="b[nome]" placeholder="Filtrar por nome">
        </div>
        <div class="col-3 col-md-3">
            <button type="submit" class="btn btn-dark text-light mb-5" disabled>Filtrar</button>
            <a class="btn btn-outline-dark btn-light text-dark mb-5" href="">Limpar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <select class="custom-select termo2" name="b[from_condominio]">
                <option value="">Filtrar condominio</option>
                <?
                foreach ($listCondominio['resultSet'] as $condominios) {
                    echo '<option value="' . $condominios['id'] . '">' . $condominios['nome_condominio'] . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-12">
        <table class="table table-hover" id="tableClientes">
            <thead class="bg-dark text-light">
                <tr>
                    <th scope="col">Condominio</th>
                    <th scope="col">Bloco</th>
                    <th scope="col">Unidade</th>
                    <th scope="col">Nome</th>
                    <th scope="col">CPF</th>
                    <th scope="col">Email</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">Endereço</th>
                    <th scope="col">Cadastro</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($result['resultSet'] as $value) { ?>
                    <tr data-id="<?= $value['id'] ?>">
                        <td><?= $value['nome_condominio'] ?></td>
                        <td><?= $value['nome_bloco'] ?></td>
                        <td><?= $value['numero_unidade'] ?></td>
                        <td><?= $value['nome'] ?></td>
                        <td><?= $value['cpf'] ?></td>
                        <td><?= $value['email'] ?></td>
                        <td><?= $value['telefone'] ?></td>
                        <td><?= $value['endereco'] ?></td>
                        <td><?= dateFormat($value['data_cadastro']) ?></td>

                        <td>
                            <a href="#" data-id="<?= $value['id'] ?>" class="removerCliente"><i class="bi bi-x-octagon text-dark"></i></a>
                            <a href="<?=$url_site?>cadastro/id/<?= $value['id'] ?>"><i class="bi bi-pencil-square text-dark"></i></a>
                        </td>
                    </tr>
                <? } ?>
                <tr>
                    <td colspan="9"></td>
                    <td class="totalRegistros">Total de Registros: <?= $totalRegistros ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row mb-5">
    <div class="col-12">
        <a class="btn btn-dark text-light" href="<?=$url_site?>cadastro">Cadastrar novo cliente</a>
    </div>
</div>
<div class="col-sm-12 mb-5">
    <?= $paginacao ?>
</div>