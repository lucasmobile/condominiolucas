<center>
    <h1 class="mb-5 mt-5">Cadastro de Blocos</h1>
</center>

<form id="formBloco" action="" method="POST">
    <div class="row mb-5">
        <div class="col-12 col-sm-9 col-md-3">
            <div class="input-group mb-3">
                
                <select class="custom-select" id="inputGroupSelect01" name="from_condominio">
                <option value="">Condomínio...</option>
                    <?
                    foreach ($listagemCondominio['resultSet'] as $condominio) {
                        echo '<option value="' . $condominio['id'] . '" ' . ($condominio['id'] == $listagemBloco['resultSet']['from_condominio'] ? 'selected' : '') . '>' . $condominio['nome_condominio'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-3 col-md-3">
            <input class="form-control" type="text" value="<?=$listagemBloco['resultSet']['nome_bloco']?>" name="nome_bloco" placeholder="Nome bloco">
        </div>
        <div class="col-12 col-sm-3 col-md-3">
            <input class="form-control" type="text" value="<?=$listagemBloco['resultSet']['qt_andares']?>" name="qt_andares" placeholder="Andares">
        </div>
        <div class="col-12 col-sm-3 col-md-3">
            <input class="form-control" type="text" value="<?=$listagemBloco['resultSet']['unidades_andar']?>" name="unidades_andar" placeholder="Unidades p/ andar">
        </div>
    </div>
    <? if($_GET['id']) { ?>
        <input type="hidden" id="editar" name="editar" value="<?=$_GET['id']?>"> 
    <? } ?>
    <div class="row">
        <div class="col-12">
            <center><button class="btn btn-dark text-light buttonEnviar" type="submit" style="width: 100%;">Cadastrar</button></center>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-5">
            <a href="<?=$url_site?>listaBlocos">Ver Lista de Blocos</a>
        </div>
    </div>
</form>