<center>
    <h2 class="mt-5">Lista de Unidades</h2>
</center>

<form method="GET" id="filtro">
    <div class="row">
        <div class="col-9 col-md-9">
            <input type="hidden" name="page" value="listaUnidades">
            <input class="form-control termo2" type="text" name="b[termo2]" placeholder="Filtrar por número">
        </div>
        <div class="col-3 col-md-3">
            <button type="submit" class="btn btn-dark text-light mb-5" disabled>Filtrar</button>
            <a class="btn btn-outline-dark btn-light text-dark mb-5" href="">Limpar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <select class="custom-select termo1" name="b[termo1]">
                <option value="">Filtrar bloco</option>
                <?
                    foreach($listagemBloco['resultSet'] as $bloc) {
                        echo '<option value="' . $bloc['nome_bloco'] . '">' . $bloc['nome_bloco'] . '</option>';
                    }
                ?>
            </select>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-12">
        <table class="table table-hover" id="tableUnidades">
            <thead class="bg-dark text-light">
                <tr>
                    <th scope="col">Bloco</th>
                    <th scope="col">Nº Unidade</th>
                    <th scope="col">Metragem Unidade</th>
                    <th scope="col">Vagas Garagem</th>
                    <th scope="col">Data Cadastro</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($result['resultSet'] as $value) { ?>
                    <tr data-id="<?= $value['id'] ?>">
                        <td><?=$value['nome_bloco']?></td>
                        <td><?=$value['numero_unidade']?></td>
                        <td><?=$value['metragem_unidade']?></td>
                        <td><?=$value['qt_vagas_garagem']?></td>
                        <td><?=dateFormat($value['data_cadastro']);?></td>
                        <td>
                            <a href="#" data-id="<?= $value['id'] ?>" class="removerUnidade"><i class="bi bi-x-octagon text-dark"></i></a>
                            <a href="<?= $url_site ?>cadastroUnidade/id/<?= $value['id'] ?>"><i class="bi bi-pencil-square text-dark"></i></a>
                        </td>
                    </tr>
                <? } ?>
                    <tr>
                        <td colspan="5"></td>
                        <td class="totalRegistros">Total de Registros: <?=($result['totalResults'] < 10 ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row mt-5 mb-5">
    <div class="col-12">
        <a class="btn btn-dark text-light mb-5" href="<?=$url_site?>cadastroUnidade">Cadastrar nova unidade</a>
    </div>
</div>
<div class="col-sm-12 mb-5">
    <?= $paginacao ?>
</div>