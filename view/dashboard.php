<center>
    <h1 class="mb-5 mt-5">Dashboard</h1>
</center>
<div class="row mb-5">
    <div class="col-12">
        <div class="jumbotron">
            <center>
                <span class="display-4">Total Cadastros</span>
                <br>
                <div class="row">
                    <div class="card mt-5" style="width: 13rem;">
                        <div class="card-body">
                            <h5 class="card-title">Administradoras</h5>
                            <p class="card-text">
                                <?=
                                  $totalRegistros['resultSet']['adm'];  
                                ?>
                            </p>
                            <a href="<?=$url_site?>listaAdministradoras" class="btn btn-primary">Lista Adm's</a>
                        </div>
                    </div>
                    <br>
                <div class="card mt-5" style="width: 13rem;">
                    <div class="card-body">
                        <h5 class="card-title">Condominios</h5>
                        <p class="card-text">
                            <?=
                                $totalRegistros['resultSet']['condominios'];
                            ?>
                        </p>
                        <a href="<?=$url_site?>listaCondominios" class="btn btn-primary">Lista Condominios</a>
                    </div>
                </div>
                <br>
                <div class="card mt-5" style="width: 13rem;">
                    <div class="card-body">
                        <h5 class="card-title">Blocos</h5>
                        <p class="card-text">
                            <?=
                                $totalRegistros['resultSet']['blocos'];
                            ?>
                        </p>
                        <a href="<?=$url_site?>listaBlocos" class="btn btn-primary">Lista Blocos</a>
                    </div>
                </div>
                <br>
                <div class="card mt-5" style="width: 13rem;">
                    <div class="card-body">
                        <h5 class="card-title">Unidades</h5>
                        <p class="card-text">
                            <?=
                                $totalRegistros['resultSet']['unidades'];
                            ?>
                        </p>
                        <a href="<?=$url_site?>listaUnidades" class="btn btn-primary">Lista Unidades</a>
                    </div>
                </div>
                <br>
                <div class="card mt-5" style="width: 13rem;">
                    <div class="card-body">
                        <h5 class="card-title">Clientes</h5>
                        <p class="card-text">
                            <?=
                                $totalRegistros['resultSet']['clientes'];
                            ?>
                        </p>
                        <a href="<?=$url_site?>listaClientes" class="btn btn-primary">Lista Clientes</a>
                    </div>
                </div>
                </div>
                <hr class="my-4">
            </center>
        </div>
    </div>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-6">
        <div class="jumbotron">
            <center>
                <span class="display-4">Número de Moradores por condomínio</span>
            </center>

            <? $i = 0;
            foreach ($listaMoradores['resultSet'] as $value) { ?>
                <button type="button" class="btn btn-<?= $coresBootStrap[$i] ?> mt-3">
                    <?= $value['nome_condominio'] ?> <span class="badge badge-light"><?= $value['totalMoradores'] ?></span>
                    <span class="sr-only"></span>
                </button>
            <? $i++;
                $i = ($i == 8) ? $i = 0 : $i;
            } ?>
            <hr class="my-4">
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="jumbotron">
            <center>
                <span class="display-4">Últimas 5 administradoras cadastradas</span>
            </center>

            <? $i = 0;
            foreach ($listaAdministradoras['resultSet'] as $value) { ?>
                <button type="button" class="btn btn-<?= $coresBootStrap[$i] ?> mt-3">
                    <?= $value['nome_administradora'] ?>
                    <span class="sr-only"></span>
                </button>
            <? $i++;
                $i = ($i == 8) ? $i = 0 : $i;
            } ?>
            <hr class="my-4">
        </div>
    </div>
</div>