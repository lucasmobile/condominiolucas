<center>
    <h1 class="mb-5 mt-5">Cadastro de Unidades</h1>
</center>

<form id="formUnidades" action="" method="POST">
<div class="row">
        <div class="col-12 col-sm-6 col-md-6">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Condominio</label>
                </div>
                <select class="custom-select" id="inputGroupSelect01" name="from_condominio">
                    <option value="">Selecione</option>
                    <?

                    foreach ($listagemCondominio['resultSet'] as $condominio) {
                        echo '<option value="' . $condominio['id'] . '"' . ($condominio['id'] == $listagemCondominio['resultSet']['from_condominio'] ? 'selected' : '') . '>' . $condominio['nome_condominio'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Bloco</label>
                </div>
                <select class="custom-select" id="inputGroupSelect01" name="from_bloco">
                    <option value="">Selecione</option>
                    <?

                    foreach ($listagemBloco['resultSet'] as $bloco) {
                        echo '<option value="' . $bloco['id'] . '"' . ($bloco['id'] == $listagemUnidade['resultSet']['from_bloco'] ? 'selected' : '') . '>' . $bloco['nome_bloco'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 col-sm-9 col-md-4">
            <input class="form-control" type="text" value="<?= $listagemUnidade['resultSet']['numero_unidade'] ?>" name="numero_unidade" placeholder="Nº Unidade">
        </div>
        <div class="col-12 col-sm-3 col-md-4">
            <input class="form-control" type="text" value="<?= $listagemUnidade['resultSet']['metragem_unidade'] ?>" name="metragem_unidade" placeholder="Metragem da Unidade">
        </div>
        <div class="col-12 col-sm-3 col-md-4">
            <input class="form-control" type="text" value="<?= $listagemUnidade['resultSet']['qt_vagas_garagem'] ?>" name="qt_vagas_garagem" placeholder="Vagas de garagem">
        </div>
    </div>
    <? if ($_GET['id']) { ?>
        <input type="hidden" id="editar" name="editar" value="<?= $_GET['id'] ?>">
    <? } ?>
    <div class="row">
        <div class="col-12">
            <center><button class="btn btn-dark text-light buttonEnviar" type="submit" style="width: 100%;">Cadastrar</button></center>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-5">
            <a href="<?=$url_site?>listaUnidades">Ver Lista de Unidades</a>
        </div>
    </div>
</form>