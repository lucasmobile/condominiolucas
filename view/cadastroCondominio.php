<center>
    <h1 class="mb-5 mt-5">Cadastro de condomínio</h1>
</center>

<form id="formCondominio" action="" method="POST">
    <div class="row mb-5">
    <div class="col-12 col-sm-6 col-md-3">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Administradora</label>
                </div>
                <select class="custom-select fromCondominio" id="inputGroupSelect01" name="from_administradoras">
                    <option value="">Selecione</option>
                    <?
                    foreach ($listagemAdministradoras['resultSet'] as $administradora) {
                        echo '<option value="' . $administradora['id'] . '"' . ($administradora['id'] == $listagemCondominio['resultSet']['from_administradoras'] ? 'selected' : '') . '>' . $administradora['nome_administradora'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-9 col-md-6">
            <input class="form-control" type="text" value="<?=$listagemCondominio['resultSet']['nome_condominio']?>" name="nome_condominio" placeholder="Nome do Condomínio">
        </div>
        <div class="col-12 col-sm-3 col-md-3">
            <input class="form-control" type="text" value="<?=$listagemCondominio['resultSet']['qt_blocos']?>" name="qt_blocos" placeholder="Quantidade de blocos">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 col-sm-9 col-md-10">
            <input class="form-control" type="text" value="<?=$listagemCondominio['resultSet']['endereco_condominio']?>" name="endereco_condominio" placeholder="Endereço">
        </div>
        <div class="col-12 col-sm-3 col-md-2">
            <input class="form-control" type="text" value="<?=$listagemCondominio['resultSet']['cep_condominio']?>" name="cep_condominio" placeholder="CEP">
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 col-md-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Estados</label>
                </div>
                <select class="custom-select" id="inputGroupSelect01" value="<?=$listagemCondominio['resultSet']['estado_condominio']?>" name="estado_condominio">
                    <?
                    foreach ($estados as $key => $value) {
                        echo '<option value="' . $key . '" '.($key == $estado_condominio ? 'selected="selected"' : '') .'>' . $value . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <input class="form-control" type="text" value="<?=$listagemCondominio['resultSet']['cidade_condominio']?>" name="cidade_condominio" placeholder="Cidade">
            
        </div>
        <div class="col-12 col-md-4">
            <input class="form-control" type="text" value="<?=$listagemCondominio['resultSet']['bairro_condominio']?>" name="bairro_condominio" placeholder="Bairro">
            
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 col-md-10">
            <input class="form-control" type="text" value="<?=$listagemCondominio['resultSet']['logradouro_condominio']?>" name="logradouro_condominio" placeholder="Logradouro">
        </div>
        <div class="col-12 col-md-2">
            <input class="form-control" type="text" value="<?=$listagemCondominio['resultSet']['numero_condominio']?>" name="numero_condominio" placeholder="Número">
        </div>
    </div>
    <? if($_GET['id']) { ?>
        <input type="hidden" id="editar" name="editar" value="<?=$_GET['id']?>"> 
    <? } ?>
    <div class="row">
        <div class="col-12">
            <center><button class="btn btn-dark text-light buttonEnviar" type="submit" style="width: 100%;">Cadastrar</button></center>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mt-5">
            <a href="<?=$url_site?>listaCondominios">Ver Lista de Condominios</a>
        </div>
    </div>
</form>