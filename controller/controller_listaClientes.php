<?
$condominio = new Condominio();
$listCondominio = $condominio->listarCondominios();

$cliente = new Cadastro();
$cliente-> pagination = 4;
if(isset($_GET['b'])) {
    $filtro = array();
    foreach($_GET['b'] as $field => $termo) {
        switch($field) {
            case 'termo1':
                $filtro['nome'] = $termo;
                break;
            case 'termo2':
                $filtro['fluccas_clientes.from_condominio'] = $termo;
                break;
        }
    }
}
$cliente-> busca = $filtro;
$result = $cliente -> listarClientes();

$paginacao = $cliente -> renderPagination($result['qtPaginas']);
$totalRegistros = ($result['totalResults'] < 10 ? '0' . $result['totalResults'] : $result['totalResults']);

?>