<?

$administradora = new Administradora();
$listAdministradoras = $administradora->listarAdministradoras();

$condominios = new Condominio();
$condominios -> pagination = 4;

if(isset($_GET['b'])) {
    $filtro = array();
    foreach($_GET['b'] as $field => $termo) {
        switch($field) {
            case 'termo1':
                $filtro['nome_condominio'] = $termo;
                break;
            case 'termo2':
                $filtro['fluccas_condominios.from_administradoras'] = $termo;
                break;
        }
    }

}

$condominios-> busca = $filtro;
$result = $condominios -> listarCondominios();

$paginacao = $condominios -> renderPagination($result['qtPaginas']);
$totalRegistros = ($result['totalResults'] < 10 ? '0' . $result['totalResults'] : $result['totalResults']);

?>