<?

$nav = array(
    'home' => 'Dashboard',
    'Cadastros' => array(
        'cadastroAdministradora' => 'Administradora',
        'cadastroCondominio' => 'Condominio',
        'cadastroConselho' => 'Conselho',
        'cadastroBloco' => 'Bloco',
        'cadastroUnidade' => 'Unidade',
        'cadastro' => 'Cliente',
        'cadastroUser' => 'Usuário'
    ),
    'Listas' => array(
        'listaAdministradoras' => 'Administradora',
        'listaCondominios' => 'Condominio',
        'listaConselho'=> 'Conselho',
        'listaBlocos' => 'Bloco',
        'listaUnidades' => 'Unidade',
        'listaClientes' => 'Clientes',
        'listaUsers' => 'Usuários'
    )
);

?>