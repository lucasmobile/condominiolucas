<?

include '../uteis.php';


$cliente = new Cadastro();
$result = $cliente -> deletaCliente($_POST['id']);
if($result) {
    $totalRegistros = $cliente -> listarClientes()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Cliente deletado com sucesso!"
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O Cliente não pode ser deletado!"
    );

    echo json_encode($result);
}

?>