<?
require "../uteis.php";
$user = new User();


$isExists = $user ->userExistis($_POST['g']['user_name']);
if($isExists['resultSet']['user_name']){
    $result = array(
        "status" => "warning",
        "msg" => "Este usuário Já existe"
    );
    echo json_encode($result);
    exit;
}



if($_POST['g']['user_password'] == $_POST['retype_password']){

    $dados = array();

    foreach($_POST['g'] as $field=>$value){
        $dados[$field] = ($field == 'user_password') ? md5($value) : $value;
    }

    if($user->salvarUser($dados)){
        $result = array(
            "status" => 'success',
            "msg" => "Usuário cadastrado com sucesso."
        );
    
    } else{
        $result = array(
            "status" => 'danger',
            "msg" => "O usuário não pode ser cadastrado"
        );
    
    }
    
} else{
    $result = array(
        "status" => "danger",
        "msg" => "As senhas digitadas não conferem!"
    );
}
echo json_encode($result);    
?>