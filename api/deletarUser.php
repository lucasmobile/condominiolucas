<?

include '../uteis.php';


$user = new User();
$result = $user -> deletaUser($_POST['id']);
if($result) {
    $totalRegistros = $user -> listarUser()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Usuário deletado com sucesso!"
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O usuário não pode ser deletado!"
    );

    echo json_encode($result);
}

?>