<?

include '../uteis.php';


$conselho = new Conselho();
$result = $conselho -> deletaConselho($_POST['id']);
if($result) {
    $totalRegistros = $conselho -> listarConselho()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Conselho deletado com sucesso!"
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O conselho não pode ser deletado!"
    );

    echo json_encode($result);
}

?>