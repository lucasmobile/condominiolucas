<?

include '../uteis.php';


$unidade = new Unidade();
$result = $unidade -> deletaUnidade($_POST['id']);
if($result) {
    $totalRegistros = $unidade -> listarUnidades()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Unidade deletada com sucesso!"
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "A unidade não pode ser deletada!"
    );

    echo json_encode($result);
}

?>