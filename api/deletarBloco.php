<?

include '../uteis.php';


$bloco = new Bloco();
$result = $bloco -> deletaBloco($_POST['id']);
if($result) {
    $totalRegistros = $bloco -> listarBlocos()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Bloco deletado com sucesso!"
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O bloco não pode ser deletado!"
    );

    echo json_encode($result);
}

?>