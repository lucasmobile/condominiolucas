<?

include "../uteis.php";

$administradora = new Administradora();

    if($administradora -> salvarAdministradora($_POST)) {
        $result = array(
            "status" => "success",
            "msg" => "Administradora cadastrada com sucesso!"
        );

        echo json_encode($result);
    } else {
        $result = array(
            "status" => "danger",
            "msg" => "Não foi possivel cadastrar a administradora!"
        );

        echo json_encode($result);
    }

?>