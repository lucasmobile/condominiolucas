<?


require "../uteis.php";

$conselho = new Conselho();
if ($conselho -> salvarConselho($_POST)) {

    $result = array(
        "status" => "success",
        "msg" => "O conselho foi Cadastrado",
    );

    echo json_encode($result);
} else {
    
    $result = array(
        "status" => "danger",
        "msg" => "Não foi possível cadastrar o Conselho",
    );

    echo json_encode($result);
}

?>