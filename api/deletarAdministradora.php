<?

include '../uteis.php';


$administradora = new Administradora();
$result = $administradora -> deletaAdministradoras($_POST['id']);
if($result) {
    $totalRegistros = $administradora -> listarAdministradoras()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Administradora deletada com sucesso!"
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "A administradora não pode ser deletada!"
    );

    echo json_encode($result);
}

?>