<?

include '../uteis.php';


$condominio = new Condominio();
$result = $condominio -> deletaCondominio($_POST['id']);
if($result) {
    $totalRegistros = $condominio -> listarCondominios()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Condominio deletado com sucesso!"
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O condominio não pode ser deletado!"
    );

    echo json_encode($result);
}

?>