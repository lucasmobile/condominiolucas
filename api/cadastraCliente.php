<?

require "../uteis.php";

$cliente = new Cadastro();
if($cliente -> salvarClientes($_POST)) {
    $result = array(
        "status" => "success",  
        "msg" => "Registro adicionado com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",  
        "msg" => "Não foi possível registrar o cadastro!",
    );

    echo json_encode($result);
}

?>