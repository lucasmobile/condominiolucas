<?
include 'uteis.php';
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>First Project</title>
</head>

<body>
    <center>
        <h3 class="mt-5">Welcome to fluccas web site!</h3>
    </center>
    <main class="container">
        <form action="<?=$url_site?>controller/controller_restrito.php" method="POST">
            <div class="form-row mt-5 mb-5">
                <div class="col-12">
                    <input class="form-control" type="text" name="user_name" placeholder="Username">
                </div>
            </div>
            <div class="form-row mt-5 mb-5">
                <div class="col-12">
                    <input class="form-control" type="password" name="user_password" placeholder="Password">
                </div>
            </div>
            <div class="form-row mt-5 mb-5">
                <div class="col-12">
                    <center>
                        <button type="submit" class="btn btn-dark text-light btn-lg">Login</button>
                    </center>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-12">
                <a href="">Forgot Password?</a>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <span><h5>Do not have an account? </h5><a href="cadastroUser.php">Create Account</a></span>
            </div>
        </div>
    </main>
    <script src="./js/jquery-3.6.0.min.js"></script>
    <script src="./js/bootstrap.bundle.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/app.js?v=<?= rand(0, 9999) ?>"></script>
    <? if(isset($_GET['msg'])) { ?>
    <script type="text/javascript">
        $(function() {
            myAlert('danger', '<?=$_GET['msg']?>', 'main');
        })
    </script>
    <? } ?>
</body>

</html>