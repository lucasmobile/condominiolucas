<?

Class Dashboard extends Dao {


    function __construct() {
        
    }

    function listaMoradoresPorCond() {
        $qry = 'SELECT
        fluccas_condominios.nome_condominio,
        COUNT(fluccas_clientes.id) AS totalMoradores
        FROM
        fluccas_clientes
        LEFT JOIN fluccas_condominios ON fluccas_condominios.id = fluccas_clientes.from_condominio
        GROUP BY fluccas_clientes.from_condominio';

        return $this -> listarData($qry);
    }

    function ultimas5AdmsCadastradas() {
        $qry = 'SELECT 
        nome_administradora 
        FROM fluccas_administradoras 
        ORDER BY data_cadastro DESC
        LIMIT 5';

        return $this -> listarData($qry);
    }

    function totalCadastros() {
        $qry = 'SELECT
        COUNT(fluccas_administradoras.id) AS adm,
        (SELECT COUNT(fluccas_condominios.id) FROM fluccas_condominios) AS condominios,
        (SELECT COUNT(fluccas_blocos.id) FROM fluccas_blocos) AS blocos,
        (SELECT COUNT(fluccas_unidades.id) FROM fluccas_unidades) AS unidades,
        (SELECT COUNT(fluccas_clientes.id) FROM fluccas_clientes) AS clientes
        FROM
        fluccas_administradoras';

        return $this -> listarData($qry, true);
    }


}

?>