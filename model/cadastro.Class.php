<?

class Cadastro extends Unidade {

    function __construct() {
    }

    function salvarClientes($dados) {
        $values = '';
        $sql = 'INSERT INTO fluccas_clientes (';
        foreach ($dados as $key => $value) {
            $sql .= '`'.$key.'`,';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .= ') VALUES ('.rtrim($values,', ').')';
        return $this -> insertData($sql);
    }

    function listarClientes($id = null){
        $qry = 'SELECT
         fluccas_clientes.id,
         fluccas_clientes.nome,
         fluccas_clientes.cpf,
         fluccas_clientes.email,
         fluccas_clientes.telefone,
         fluccas_clientes.endereco,
         fluccas_clientes.data_cadastro,
         fluccas_clientes.from_condominio, 
         fluccas_clientes.from_bloco,
         fluccas_clientes.from_unidade,
         fluccas_condominios.nome_condominio,
         fluccas_blocos.nome_bloco,
         fluccas_unidades.numero_unidade
         FROM
         fluccas_clientes
         INNER JOIN fluccas_condominios ON fluccas_condominios.id = fluccas_clientes.from_condominio
         INNER JOIN fluccas_blocos ON fluccas_blocos.id = fluccas_clientes.from_bloco
         INNER JOIN fluccas_unidades ON fluccas_unidades.id = fluccas_clientes.from_unidade';

        $contaTermos = count($this -> busca);

        $isNull = false;

        if($contaTermos > 0){
            
            $i = 0;
            foreach($this -> busca as $field => $termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .= ' WHERE fluccas_clientes.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique);
    }

    function editarClientes($dados) {
        $sql = 'UPDATE fluccas_clientes SET ';

        foreach ($dados as $key => $value) {
            if($key != 'editar') {
                $sql .= "`".$key."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this -> updateData($sql);
    }

    function deletaCliente($id) {
        $qry = 'DELETE FROM fluccas_clientes WHERE id='.$id;
        return $this -> deletar($qry);
    }
}