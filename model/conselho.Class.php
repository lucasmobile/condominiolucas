<?

Class Conselho extends Condominio {


    function __construct() {
        
    }

    function salvarConselho($dados) {
        $values = '';
        $qry = 'INSERT INTO fluccas_conselho (';
        foreach($dados as $key => $value){
            $qry .=''.$key.', ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);

    }

    function listarConselho($id = null) {
        $qry = 'SELECT 
        fluccas_conselho.id,
        fluccas_conselho.nome,
        fluccas_conselho.funcao,
        fluccas_conselho.data_cadastro,
        fluccas_conselho.from_condominio
        FROM fluccas_conselho
        INNER JOIN fluccas_condominios ON fluccas_condominios.id = fluccas_conselho.from_condominio';
        if($id) {
            $qry .= ' WHERE id = '.$id;
            $unique = true;
        }
        return $this -> listarData($qry, $unique); 
    }

    function listarFuncao() {
        $qry = 'SELECT DISTINCT funcao FROM fluccas_conselho WHERE funcao';
        $unique = false;
        return $this -> listarData($qry, $unique);
    }

    function editarConselho($dados) {
        $sql = 'UPDATE fluccas_conselho SET ';

        foreach ($dados as $key => $value) {
            if($key != 'editar') {
                $sql .= "`".$key."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this -> updateData($sql);
    }

    function deletaConselho($id) {
        $qry = 'DELETE FROM fluccas_conselho WHERE id='.$id;
        return $this -> deletar($qry);
    }
}

?>