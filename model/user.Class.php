<?

Class User extends Dao {

    protected $user_name;
    protected $user_password;

    function __construct() {
        
    }

    function salvarUser($dados) {
        $values ='';
          $qry = 'INSERT INTO fluccas_users (';
          foreach($dados as $ch=> $value){
              $qry .='`'.$ch.'`, ';
              $values .= "'".$value."', ";
          }
          $qry = rtrim($qry,', ');
          $qry .= ') VALUES ('.rtrim($values,', ').')';
          return $this->insertData($qry);
        
        return $this -> insertData($qry);
    }

    function userExistis($user){
        $qry = "SELECT user_name FROM fluccas_users WHERE user_name = '".$user."'";
        return $this->listarData($qry,true);
    }

    function listarUsers($id = null) {
        $qry = 'SELECT * FROM fluccas_users';
        if($this -> busca) {
            $qry = $qry.' WHERE user_name LIKE "%'.$this -> busca.'%"';
        }
        if($id) {
            $qry .= ' WHERE fluccas_users.id = '.$id;
            $unique = true;
        }
        return $this -> listarData($qry, $unique, 4); 
    }

    function editarUser($dados) {
        $sql = 'UPDATE fluccas_users SET ';

        foreach ($dados as $key => $value) {
            if($key != 'editar') {
                $sql .= "`".$key."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this -> updateData($sql);
    }

    function deletaUser($id) {
        $qry = 'DELETE FROM fluccas_users WHERE id='.$id;
        return $this -> deletar($qry);
    }
}

?>