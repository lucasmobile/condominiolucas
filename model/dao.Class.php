<?

class Dao {

    public static $instance;
    public $pagination = 0;
    public $busca = array();

    function __construct() {
        
    }

    public static function getInstance() {
        if(!isset(self::$instance)) {
            self::$instance = new Dao();
        }
        return self::$instance;
    }

    public function insertData($qry) {
        try {
            $sql = ConnectDB::getInstance()->prepare($qry);
            if($sql -> execute()) {
                return true;
            }
            
        } catch (Exception $e) {
            legivel($e);
            echo ' query: '.$qry;
            return false;
        }
    }

    public function updateData($qry) {
        try {
            $sql = ConnectDB::getInstance()->prepare($qry);
            $sql -> execute();
            return true;
        } catch (Exception $e) {
            legivel($e);
            echo ' query: '.$qry;
            return false;
        }
    }

    public function deletar($qry) {
        try {
            $sql = ConnectDB::getInstance()->prepare($qry);
            $sql -> execute();
            return array (
                'totalResults' => $sql -> rowCount()
            );
        } catch (Exception $e) {
            legivel($e);
            echo ' query: '.$qry;
            return false;
        }
    }

    public function listarData($qry, $unique=false) {
        try {
            if($this -> pagination > 0){
                $sql = ConnectDB::getInstance()->prepare($qry);
                $sql->execute();
                $totalResults = $sql->rowCount();
                $totalPaginas = ceil($totalResults / $this -> pagination);
                $qry = $qry.$this -> limitPagination($this -> pagination);
            }
            $sql = ConnectDB::getInstance()->prepare($qry);
            $sql->execute();
            if(!$unique){
                $resultSet = $sql -> fetchAll(PDO::FETCH_ASSOC);
            }else{
                $resultSet =  $sql -> fetch(PDO::FETCH_ASSOC);
            }

            return array(
                'resultSet' => $resultSet,
                'totalResults' => ($totalResults ? $totalResults : $sql->rowCount()),
                'qtPaginas' => ($totalPaginas ? $totalPaginas : 0)
            );
        } catch (Exception $e) {
            legivel($e);
            echo $qry;
            return false;
        }
    }

    function limitPagination($qtRegistros) {
        $pagAtual = ($_GET['pagina']) ? $_GET['pagina'] : 1;
        $inicio = ($qtRegistros * $pagAtual) - $qtRegistros;
        return " LIMIT ".$inicio.", ".$qtRegistros;

    }

    function renderPagination($qtPaginas) {
        global $url_site;
        $pagAtual = ($_GET['pagina']) ? $_GET['pagina'] : 1;
        $url = $url_site.$_GET['page'].'/'.trataURL($_GET['b']);
        $estrutura = ' 
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link bg-dark text-light" href='.$url.'>First</a>
            </li>';
        
            $incremento = ($pagAtual > 3 ? $pagAtual-3 : 1);
            for ($i = $incremento; $i < $pagAtual; $i++) {
                $estrutura .= '<li class="page-item"><a class="page-link bg-light text-dark" href='.$url.'pagina/'.$i.'>'.$i.'</a></li>';
            }

        $estrutura .= '<li  class="page-item"><a class="page-link bg-dark text-light" href='.$url.'pagina/'.$pagAtual.'>'.$pagAtual.'</a></li>';
        
        
        if($pagAtual <= $qtPaginas) {
            $calculaIncremento = $qtPaginas - $pagAtual;
            $incremento = ($calculaIncremento > 4 ? 3 : $calculaIncremento);
            for ($i = $pagAtual + 1; $i <= $pagAtual + $incremento; $i++) {
                $estrutura .= '<li class="page-item"><a class="page-link bg-light text-dark" href='.$url.'pagina/'.$i.'>'.$i.'</a></li>';
            }
        }
            
        $estrutura .=' <li class="page-item">
                        <a class="page-link bg-dark text-light" href='.$url.'pagina/'.$qtPaginas.'>Last</a>
                      </li>
                        </ul>
                      </nav> ' ;

        return $estrutura;
    }
}

?>