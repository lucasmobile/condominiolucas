<?

Class Condominio extends Administradora {

    function __construct() {

    }

    function salvarCondominio($dados) {
        $values = '';
        $sql = 'INSERT INTO fluccas_condominios (';
        foreach ($dados as $key => $value) {
            $sql .= '`'.$key.'`,';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .= ') VALUES ('.rtrim($values,', ').')';
        return $this -> insertData($sql);
    }
    
    function listarCondominios($id = null) {
        $qry = 'SELECT 
        fluccas_condominios.id,
        fluccas_condominios.nome_condominio,
        fluccas_condominios.qt_blocos,
        fluccas_condominios.endereco_condominio,
        fluccas_condominios.cep_condominio,
        fluccas_condominios.estado_condominio,
        fluccas_condominios.cidade_condominio,
        fluccas_condominios.bairro_condominio,
        fluccas_condominios.logradouro_condominio,
        fluccas_condominios.numero_condominio,
        fluccas_condominios.data_cadastro,
		fluccas_condominios.from_administradoras, 
        fluccas_administradoras.nome_administradora 
        FROM fluccas_condominios 
        LEFT JOIN fluccas_administradoras ON fluccas_administradoras.id = fluccas_condominios.from_administradoras';

        
        $contaTermos = count($this -> busca);

        $isNull = false;

        if($contaTermos > 0){
            
            $i = 0;
            foreach($this -> busca as $field => $termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }

        if($id) {
            $qry .= ' WHERE fluccas_condominios.id = '.$id;
            $unique = true;
        }


        return $this -> listarData($qry, $unique, 4); 
    }

    function editarCondominios($dados) {
        $sql = 'UPDATE fluccas_condominios SET';

        foreach ($dados as $key => $value) {
            if($key != 'editar') {
                $sql .= "`".$key."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .= ' WHERE fluccas_condominios.id='.$dados['editar'];

        return $this -> updateData($sql);
    }

    function deletaCondominio($id) {
        $qry = 'DELETE FROM fluccas_condominios WHERE id='.$id;
        return $this -> deletar($qry);
    }
}

?>