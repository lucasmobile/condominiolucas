<?

Class Administradora extends Dao {

    function __construct() {

    }

    function salvarAdministradora($dados) {
        $values = '';
        $sql = 'INSERT INTO fluccas_administradoras (';
        foreach ($dados as $key => $value) {
            $sql .= '`'.$key.'`,';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .= ') VALUES ('.rtrim($values,', ').')';
        return $this -> insertData($sql);
    }

    function listarAdministradoras($id = null) {
        $qry = 'SELECT * FROM fluccas_administradoras';
        if($this -> busca) {
            $qry = $qry.' WHERE nome_administradora LIKE "%'.$this -> busca.'%"';
        }
        if($id) {
            $qry .= ' WHERE id = '.$id;
            $unique = true;
        }
        return $this -> listarData($qry, $unique, 4); 
    }

    function editarAdministradoras($dados) {
        $sql = 'UPDATE fluccas_administradoras SET';

        foreach ($dados as $key => $value) {
            if($key != 'editar') {
                $sql .= "`".$key."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this -> updateData($sql);
    }

    function deletaAdministradoras($id) {
        $qry = 'DELETE FROM fluccas_administradoras WHERE id='.$id;
        return $this -> deletar($qry);
    }
}

?>