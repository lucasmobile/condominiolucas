<?

Class Bloco extends Condominio {

    function __construct() {

    }

    function salvarBloco($dados) {
        $values = '';
        $sql = 'INSERT INTO fluccas_blocos (';
        foreach ($dados as $key => $value) {
            $sql .= '`'.$key.'`,';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .= ') VALUES ('.rtrim($values,', ').')';
        return $this -> insertData($sql);
    }

    function listarBlocos($id = null) {
        $qry = 'SELECT
        fluccas_blocos.id,
        fluccas_condominios.nome_condominio,
        fluccas_blocos.nome_bloco,
        fluccas_blocos.qt_andares,
        fluccas_blocos.unidades_andar,
        fluccas_blocos.data_cadastro,
        fluccas_blocos.from_condominio
        FROM
        fluccas_blocos
        INNER JOIN fluccas_condominios ON fluccas_condominios.id = fluccas_blocos.from_condominio';
        $contaTermos = count($this -> busca);

        $isNull = false;

        if($contaTermos > 0){
            
            $i = 0;
            foreach($this -> busca as $field => $termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }
        if($id) {
            $qry .= ' WHERE fluccas_blocos.id = '.$id;
            $unique = true;
        }
        return $this -> listarData($qry, $unique, 4); 
    }

    function getBlocoFromCond($id) {
        $qry = 'SELECT id, nome_bloco FROM fluccas_blocos WHERE from_condominio ='.$id;
        return $this -> listarData($qry);
    }

    function editarBlocos($dados) {
        $sql = 'UPDATE fluccas_blocos SET';

        foreach ($dados as $key => $value) {
            if($key != 'editar') {
                $sql .= "`".$key."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .= ' WHERE fluccas_blocos.id='.$dados['editar'];

        return $this -> updateData($sql);
    }

    function deletaBloco($id) {
        $qry = 'DELETE FROM fluccas_blocos WHERE id='.$id;
        return $this -> deletar($qry);
    }
}

?>