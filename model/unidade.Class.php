<?

Class Unidade extends Bloco {

    function __construct() {

    }

    function salvarUnidade($dados) {
        $values = '';
        $sql = 'INSERT INTO fluccas_unidades (';
        foreach ($dados as $key => $value) {
            $sql .= '`'.$key.'`,';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql,', ');
        $sql .= ') VALUES ('.rtrim($values,', ').')';
        return $this -> insertData($sql);
    }

    function listarUnidades($id = null) {
        $qry = 'SELECT
        fluccas_unidades.id,
        fluccas_blocos.nome_bloco,
        fluccas_unidades.numero_unidade,
        fluccas_unidades.metragem_unidade,
        fluccas_unidades.qt_vagas_garagem,
        fluccas_unidades.data_cadastro,
        fluccas_unidades.from_bloco,
        fluccas_unidades.from_condominio
        FROM
        fluccas_blocos
        INNER JOIN fluccas_unidades ON fluccas_unidades.from_bloco = fluccas_blocos.id';
        $contaTermos = count($this -> busca);

        $isNull = false;

        if($contaTermos > 0){
            
            $i = 0;
            foreach($this -> busca as $field => $termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }
        if($id) {
            $qry .= ' WHERE fluccas_unidades.id = '.$id;
            $unique = true;
        }
        return $this -> listarData($qry, $unique, 2); 
    }

    function editarUnidades($dados) {
        $sql = 'UPDATE fluccas_unidades SET';

        foreach ($dados as $key => $value) {
            if($key != 'editar') {
                $sql .= "`".$key."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this -> updateData($sql);
    }

    function deletaUnidade($id) {
        $qry = 'DELETE FROM fluccas_unidades WHERE id='.$id;
        return $this -> deletar($qry);
    }

    function getUnidFromBloco($id) {
        $qry = 'SELECT id, numero_unidade FROM fluccas_unidades WHERE from_bloco ='.$id;
        return $this -> listarData($qry, false);
    }
}

?>