<?
include 'uteis.php';

$user = new Restrito();
if(!$user -> acesso()) {
    header("Location: ".$url_site."login.php");
}
if($_GET['page'] == 'logout') {
    if($user -> logout()) {
        header('Location: '.$url_site.'login.php');
    }
}

// legivel($_GET);

?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$url_site?>css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>First Project</title>
</head>

<body style="background-color: rgb(236, 236, 236);">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?=$url_site?>home"><i class="bi bi-apple" style="font-size: 2rem;"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        
        <? foreach($nav as $ch=>$menu){
            if(is_array($menu)){?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="<?=$url_site.$ch?>" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                    <?=$ch?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <? foreach($menu as $ch2=>$submenu){ ?>
                    <a class="dropdown-item" href="<?=$url_site.$ch2?>"><?=$submenu?></a>
                    <? } ?>
                </div>
                </li>
           <? } else {?>
            <li class="nav-item ">
                <a class="nav-link" href="<?=$url_site.$ch?>"><?=$menu?> </a>
            </li>

         <? } ?>
    <? } ?>
    </ul>
    <ul>
        <li><span><a href="<?$url_site?>logout"><i class="bi bi-box-arrow-right text-light" style="font-size: 2rem;"></a></i></span></li>
    </ul>
    <?
    $nome = explode(' ', $_SESSION['USUARIO']['user_name'])
    ?>
    <small class="text-light">Olá <?=$nome[0];?>, Seja bem vindo ao fluccas website</small>
  </div>
</nav>
    <main class="container">
        <?
        switch ($_GET['page']) {
            case '':
            case 'home':
            case 'logo':
                require "controller/controller_dashboard.php";
                include "view/dashboard.php";
                break;
            default:
                require 'controller/controller_'.$_GET['page'].'.php';
                require 'view/'.$_GET['page'].'.php';
                break;
        }
        ?>
    </main>
    <footer class="footer mt-auto py-3 bg-dark text-light mt-5" style="position: fixed; bottom: 0; width: 100%; padding: 10px;">
        <div class="row">
            <div class="col-4">
                <center><span>@PHP</span></center>
            </div>
            <div class="col-4">
                <center><span><i class="bi bi-whatsapp"></i> (47) 98913-1785</span></center>

            </div>
            <div class="col-4">
                <center><span>Developed by Fluccas</span></center>
            </div>
        </div>
    </footer>
    <script> var url_site = '<?=$url_site?>';</script>
    <script src="<?=$url_site?>./js/jquery-3.6.0.min.js"></script>
    <script src="<?=$url_site?>./js/bootstrap.bundle.min.js"></script>
    <script src="<?=$url_site?>./js/bootstrap.min.js"></script>
    <script src="<?=$url_site?>./js/app.js?v=<?= rand(0, 9999) ?>"></script>
</body>

</html>