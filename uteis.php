<?


session_start();
error_reporting('E_FATAL | E_PARSE');
// session_destroy();

$local_dir = 'condominiolucas/';
$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/';

$url_site = $base_url.$local_dir;

$full_path = $_SERVER['DOCUMENT_ROOT'].'/';
$full_path .= $local_dir;

$includes = $full_path.'includes/';
$model = $full_path.'model/';
$controller = $full_path.'controller/';
$view = $full_path.'view/';


// set_error_handler('trataErros');
require $model."connectDB.Class.php";
require $model."dao.Class.php";
require $model."administradora.Class.php";
include $model."condominio.Class.php";
include $model."conselho.Class.php";
include $model."bloco.Class.php";
include $model."unidade.Class.php";
include $model."cadastro.Class.php";
require $model."restrito.Class.php";
include $model.'dashboard.Class.php';
include $model.'user.Class.php';
include $controller.'controller_navigation.php';

include $includes."vars.php";
include $includes.'global-functions.php';


function dateFormat($d, $tipo = true) {
    if(!$d) {
        return 'Sem data';
    }

    if($tipo = true) {
        $hora = explode(' ', $d);
        $data = explode('-',$hora[0]);
        return $data[2].'/'.$data[1].'/'.$data[0].' '.$hora[1];
    } else {
        $hora = explode(' ', $d);
        $data = explode('/',$hora[0]);
        return $data[2].'-'.$data[1].'/'.$data[0].' '.$hora[1];
    }
}

define('DEBUG', true);
function legivel($var,$width = '250',$height = '400') {
    if(DEBUG) {
        echo "<pre>";
        if(is_array($var)) {
            print_r($var);
        } else {
            print($var);
        }
        echo "</pre>";
        
    }
}

function trataURL($params = array()) {
    $url = ($_GET['b']) ? 'busca/' : '';
    foreach($params as $value) {
        $url .= $value.'/';
    }
    return $url;
}

// function trataErros($errorNum, $errorStr, $errorFile, $errorLine) {

//     echo $errorNum.' x '.$errorStr.' x '.$errorFile.' x '.$errorLine;

// }

?>