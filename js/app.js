$(function() {

    function selectPopulation(seletor, dados, field) {
        estrutura = '<option value ="">Selecione...</option>';

        for(let i = 0; i < dados.length; i++) {
            estrutura += '<option value ="'+dados[i].id+'">'+dados[i][field]+'</option>';
        }

        $(seletor).html(estrutura);
    }

    $('.fromCondominio').change(function() {
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success: function(data) {
                selectPopulation('.fromBloco', data.resultSet, 'nome_bloco');
            }
        })
    });

    $('.fromConselho').change(function() {
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listConselhos.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success: function(data) {
                selectPopulation('.fromConselho', data.resultSet, 'funcao');
            }
        })
    });

    $('.fromBloco').change(function() {
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listUnidades.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado},
            success: function(data) {
                selectPopulation('.fromUnidade', data.resultSet, 'numero_unidade');
            }
        })
    });

    $('#formClientes').submit(function() {
        var editar = $('#editar').val();

        var url;
        var urlRedir;
        if(editar) {
            url = url_site+'api/editarCliente.php';
            urlRedir = url_site+'listaClientes';
        } else {
            url = url_site+'api/cadastraCliente.php';
            urlRedir = url_site+'cadastro';
        }

        // $('.buttonEnviar').attr('disabled','disabled');
        
        $.ajax({
            dataType: 'json',
            url: url,
            type: 'POST',
            data: $(this).serialize(),
            success : function(data) {
                if(data.status == 'success') {
                    //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }

        })

        return false;
    });

    $('#tableClientes').on('click', '.removerCliente', function() {
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url : url_site+'api/deletarCliente.php',
            dataType : 'json',
            type : 'POST',
            data : { 
                id: idRegistro 
            },
            success : function(data) {
                if(data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'listaClientes', 'prepend');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })

        return false;
    });


    $('#formCondominio').submit(function() {
        var editar = $('#editar').val();

        var url;
        var urlRedir;
        if(editar) {
            url = url_site+'api/editarCondominio.php';
            urlRedir = url_site+'listaCondominios';
        } else {
            url = url_site+'api/cadastraCondominio.php';
            urlRedir = url_site+'cadastroCondominio';
        }

        // $('.buttonEnviar').attr('disabled','disabled');
        
        $.ajax({
            url : url,
            dataType : 'json',
            type : 'POST',
            data : $(this).serialize(),
            success : function(data) {
                console.log(data);
                if(data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    })

    $('#tableCondominios').on('click', '.removerCondominio', function() {
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url : url_site+'api/deletarCondominio.php',
            dataType : 'json',
            type : 'POST',
            data : { 
                id: idRegistro 
            },
            success : function(data) {
                
                if(data.status == 'success') {
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', null, 'prepend');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })

        return false;
    })

    $('#formConselho').submit(function() {
        var editar = $('#editar').val();

        var url;
        var urlRedir;
        if(editar) {
            url = url_site+'api/editarConselho.php';
            urlRedir = url_site+'listaConselho';
        } else {
            url = url_site+'api/cadastraConselho.php';
            urlRedir = url_site+'cadastroConselho';
        }

        // $('.buttonEnviar').attr('disabled','disabled');
        
        $.ajax({
            url : url,
            dataType : 'json',
            type : 'POST',
            data : $(this).serialize(),
            success : function(data) {
                console.log(data);
                if(data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    })


    $('#formBloco').submit(function() {
        var editar = $('#editar').val();

        var url;
        var urlRedir;
        if(editar) {
            url = url_site+'api/editarBloco.php';
            urlRedir = url_site+'listaBlocos';
        } else {
            url = url_site+'api/cadastraBloco.php';
            urlRedir = url_site+'cadastroBloco';
        }

        // $('.buttonEnviar').attr('disabled','disabled');
        
        $.ajax({
            url : url,
            dataType : 'json',
            type : 'POST',
            data : $(this).serialize(),
            success : function(data) {
                console.log(data);
                if(data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    })

    $('#tableBlocos').on('click', '.removerBloco', function() {
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url : url_site+'api/deletarBloco.php',
            dataType : 'json',
            type : 'POST',
            data : { 
                id: idRegistro 
            },
            success : function(data) {
                
                if(data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'listaBlocos', 'prepend');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })

        return false;
    });

    $('#formUnidades').submit(function() {
        var editar = $('#editar').val();

        var url;
        var urlRedir;
        if(editar) {
            url = url_site+'api/editarUnidade.php';
            urlRedir = url_site+'listaUnidades';
        } else {
            url = url_site+'api/cadastraUnidade.php';
            urlRedir = url_site+'cadastroUnidade';
        }

        // $('.buttonEnviar').attr('disabled','disabled');
        
        $.ajax({
            url : url,
            dataType : 'json',
            type : 'POST',
            data : $(this).serialize(),
            success : function(data) {
                console.log(data);
                if(data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    });

    $('#tableUnidades').on('click', '.removerUnidade', function() {
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url : url_site+'api/deletarUnidade.php',
            dataType : 'json',
            type : 'POST',
            data : { 
                id: idRegistro 
            },
            success : function(data) {
                
                if(data.status == 'success') {
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', null, 'prepend');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })

        return false;
    });

    $('#formAdministradoras').submit(function() {
        var editar = $('#editar').val();

        var url;
        var urlRedir;
        if(editar) {
            url = url_site+'api/editarAdministradora.php';
            urlRedir = url_site+'listaAdministradoras';
        } else {
            url = url_site+'api/cadastraAdministradora.php';
            urlRedir = url_site+'cadastroAdministradora';
        }

        // $('.buttonEnviar').attr('disabled','disabled');
        
        $.ajax({
            url : url,
            dataType : 'json',
            type : 'POST',
            data : $(this).serialize(),
            success : function(data) {
                console.log(data);
                if(data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    });

    $('#tableAdministradoras').on('click', '.removerAdministradora', function() {
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url : url_site+'api/deletarAdministradora.php',
            dataType : 'json',
            type : 'POST',
            data : { 
                id: idRegistro 
            },
            success : function(data) {
                
                if(data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'listaAdministradoras', 'prepend');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })

        return false;
    });

    $('#formUsers').submit(function() {
        var editar = $('#editar').val();

        var url;
        var urlRedir;
        if(editar) {
            url = url_site+'api/editarUser.php';
            urlRedir = url_site+'listaUsers';
        } else {
            url = url_site+'api/cadastraUser.php';
            urlRedir = url_site+'cadastroUser';
        }

        // $('.buttonEnviar').attr('disabled','disabled');
        
        $.ajax({
            url : url,
            dataType : 'json',
            type : 'POST',
            data : $(this).serialize(),
            success : function(data) {
                console.log(data);
                if(data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    });

    $('#tableUsers').on('click', '.removerUser', function() {
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url : url_site+'api/deletarUser.php',
            dataType : 'json',
            type : 'POST',
            data : { 
                id: idRegistro 
            },
            success : function(data) {
                
                if(data.status == 'success') {
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', null, 'prepend');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })

        return false;
    });

    $('#formConselho').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editarConselho.php';
            urlRedir = url_site+'listaConselho';
        } else{
            url = url_site+'api/cadastraConselho.php';
            urlRedir = url_site+'cadastroConselho';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', urlRedir);
                } else {
                    myAlert(data.status,data.msg,'main', urlRedir);
                }
            }
        });

        return false;
    });

    $('#tableConselho').on('click', '.removerConselho', function() {
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url : url_site+'api/deletarConselho.php',
            dataType : 'json',
            type : 'POST',
            data : { 
                id: idRegistro 
            },
            success : function(data) {
                
                if(data.status == 'success') {
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', null, 'prepend');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })

        return false;
    });

    $('#filtro').submit(function() {
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();

        window.location.href = url_site + pagina + '/busca/' + termo1 + '/' + termo2;

        return false;
    });

    $('.termo1, .termo2').on('keyup focusout change',function() {
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();
        if(termo1 || termo2) {
            $('button[type="submit"]').prop('disabled', false);
        } else {
            $('button[type="submit"]').prop('disabled', true);
        }
    });

})

function myAlert(tipo, mensagem, pai, url, position) {
    position = (position == undefined) ? position = 'append' : position = position;
    url = (url == undefined) ? url == '' : url = url;
    componente = '<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>'

    if(position == 'append') {
        $(pai).append(componente);
    } else {
        $(pai).prepend(componente);
    }
    
    setTimeout(function() {
        $(pai).find('div.alert').remove();
        if(tipo == 'success' && url) {
           setTimeout(function() {
                window.location.href = url;
           },300) 
        }
    },2000)
}